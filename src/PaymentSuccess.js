import React from 'react';

function PaymentSuccess() {
    return (
        <div style={{backgroundcolor: "whitesmoke"}}>
            <div className ="container mt-5">
                <div className="row text-left">
                   <button className="btn btn-primary" style={{color:"white",maxWidth:"203px"}} href="/#"> <h5>Quay Lại Trang Chủ</h5> </button>
                </div>
            </div>
            <div  className="container mt-5 mb-5 bg-white">
               <h2 className="pt-5 text-center" style={{color:"green"}}>THANH TOÁN THÀNH CÔNG</h2>
                <div className="row ">
                <p className="pt-3 text-center">Cảm ơn bạn. Đơn đặt hàng của bạn đã được nhận, Nhân Viên sẽ sơm soạn đơn đặt hàng của bạn.</p>
                <div className="text-center">
                <img style={{width:"200px"}}  src="https://scontent-hkg4-2.xx.fbcdn.net/v/t1.15752-9/173230510_1216984905426913_7821080656904254425_n.png?_nc_cat=111&ccb=1-3&_nc_sid=ae9488&_nc_ohc=stKGIs7WU2sAX8IDJvX&_nc_ht=scontent-hkg4-2.xx&oh=d1644442a2ed99440622c9a0ce459a59&oe=60CDBDA5">
                    
                    </img>
                </div>
                </div>
            </div>
        </div>
    );
}

export default PaymentSuccess;